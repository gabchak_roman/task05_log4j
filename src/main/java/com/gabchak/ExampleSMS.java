package com.gabchak;

import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

class ExampleSMS {

    /***
     * ACCOUNT_SID
     */
    public static final String ACCOUNT_SID = "AC7597236c9e939d80081079bc7033fb30";
    /***
     * AUTH_TOKEN
     */
    public static final String AUTH_TOKEN = "424391d48c392ca8b2d39431cbbfb378";

    public static void send(String str) {
        Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
        Message message = Message
                .creator(new PhoneNumber("+380978087574"), //my phone number
                        new PhoneNumber("+12562448089"), str).create(); //twillio number
    }
}
